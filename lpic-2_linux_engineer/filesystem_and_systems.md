# Filesystem and Devices

## fstab

tools

* blkid
* mount

Exlanations of fstab fields:

`dump` 0 | 1

Used by old backup program that is not used anymore. Should always be 0.

0: Don't make backups
1: Make backups

`pass` 0 | 1 | 2

0: Don't check for errors on boot
1: This is a critical drive
2: Secondary drive that is not critical to boot. The operating system does not depend on it

Check if fstab file will work as intended

```sh
# Run fstab and let it do it's thing
sudo mount -a
```
