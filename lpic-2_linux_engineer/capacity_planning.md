# ITPro TV Linux engineering

## Capacity planning

tools

* top
* sar
* uptime
* w
* ab (Apache Benchmark)

### sar

Monitor over longer time

```sh
sudo apt install sysstat

# Enable sysstat
sudoedit /etc/default/sysstat
ENABLED="true"

# Modify the cronjob
sudoedit /etc/crond.d/sysstat

# Log files
/var/log/sysstat

# Show sar reports
# Remember flags with 'urban' -> 'urbn'

# CPU
sar -u
# RAM
sar -r
# I/O
sar -b
# Network I/O
sar -n TCP
```

## Measuring CPU activity

tools

* top
* ps
* pstree
* pmap
* lsof
* mpstat

Check only one process activity
H display individual threads

```sh
top -Hp $PID
```

Show all processes currently running

a Show all processes

u Show which user is attached to process

x Show processes not attached to a tty (like system services)

```sh
ps aux
```

Show processes and their child processes in a tree

```sh
pstree
```

Report memory map of a process

```sh
sudo pmap $PID
```

List which files are opened by a process
If for example a process is writing to a log file it will be shown here

```sh
sudo lsof -p $PID
```

You can also show which process has a file opened

```sh
sudo lsof /var/log/mylogfile
# On btrfs
sudo lsof | grep /var/log/mylogfile
```

Show CPU activity like in the sar report but in real-time

```sh
mpstat $INTERVAL_IN_SECONDS

mpstat 2
```

## Measuring Memory Usage

tools

* free
* sar
* vmstat

Show available memory

```sh
free -h
# Update in real-time
free -hs 2
```

Log memory usage

```sh
sar -r
# Swap usage
sar -S
# Page swap in and out
# How much has been written in or out from swap
sar -W
# Page RAM in and out
# Same as above but for RAM
sar -B
```

Virtual memory

```sh
vmstat
# Show in Megabytes
vmstat --unit M
# Update in real-time
vmstat --unit M $SECONDS
vmstat --unit M 2
```

## Measuring Disk Activity

tools

* iotop
* lsof
* iostat
* sar

Generate disk reads

```sh
sudo md5sum /dev/mmcblk0p2
```

Get an overview of disk activity

```sh
sudo iotop
# Accumulative
sudo iotop -a
```

Use lsof to check which storage files are used by a process.
Common directories are `/var/log` and `/var/lib`.
If a process is using too much I/O, moving the dir to a separate partition or
to a RAID array may save a lot of I/O

```sh
sudo lsof -p $PID
```

Check disk activity for each device

```sh
iostat
# More details
iostat -x
# Refresh every second
iostat -xt 1
```

Historical data

```sh
sar -b
# Show for a particular day in the month, for example the 18th between 6 A.M. and 12 P.MG.
sar -b -f /var/log/sysstat/sa18 -s 06:00:00 -e 12:00:00
```

## Measuring Network Activity

tools

* iftop
* sar
* mtr

Current activity

```sh
sudo iftop -n -i eth0
```

Historical activity

```sh
sar -n DEV
```

MTR is like traceroute on steroids. It can be used on laptops or desktops with a GUI. On a server it would be all CLI.
MTR keeps track of packet loss and ping time to all the different routers on the way to the host

```sh
mtr www.google.com
```

Show automonous systems. The ASN column shows which network the hostname or IP address belongs to.

```sh
mtr -z
```

Check all current connections

```sh
ss -tulpn
```

```sh
sudo lsof -iTCP -sTCP:ESTABLISHED
```
