# Linux Kernel

## Kernel Components

You can find the kernel in `/boot` named something like `vmlinuz`

The module loaded by the kernel can be find in `/var/lib/modules`

## Kernel Modules

tools

* lsmod
* modinfo
* insmod
* rmmod
* modprobe
* lsusb
* lspci
* lsdev

List loaded modules

```sh
lsmod
```

Show module info

```sh
modinfo $MODULENAME
modinfo btrfs
```

Load module

```sh
sudo insmod /lib/modules/5.14.3-arch1-1/kernel/path/to/mymodule.ko
# Or
sudo modprobe -a mymodule

# Check if module was loaded
lsmod | grep mymodule

```

Unload module

```sh
sudo rmmod mymodule
# Or
sudo modprobe -r mymodule
```

Check if module was loaded correctly depending on hardware type

```sh
# USB
lsusb
# PCI
# If a row says Unknown device, it probably means it is missing a module
lspci
# For devices that are not a bus (usb bus, pci bus) like virtual devices
lsdev
```

How to hard code device name with udev

```sh
# Change device name of ethernet interface to eth0
cd /etc/udev/rules.d/
touch 70-eth-interface.rules
vim 70-eth-interface.rules

SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="80:3f:5d:01:6b:71", ATTR{type}=="1", KERNEL=="enp0s20f0u1u2i5", NAME=="eth0"
:wq

# For it to take effect you should reboot, but it is also possible to do this:
sudo udevadm control --reload-rules && udevadm trigger
```

## Monitoring the Kernel

tools

* sysctl

`/proc` is a pseudo-directory where stuff are represented as files.

The numbered directory-names here are process ID:s where you can check currently running processes.

```sh
cat /proc/${PID}/environ
```

Some example files in `/proc` that can be viewed with another tool

```sh
# /proc/sys/kernel/osrelease
uname -r
# /proc/uptime
uptime
# /proc/modules
lsmod
```

Modifiy max limit number of open files

```sh
# Check how many are open
cat /proc/sys/fs/file-nr
# Check max limit
/proc/sys/fs/file-max
# You can modify the file directly, but it's better to use sysctl
# View max files
sudo sysctl fs.file-%ax
# Modify
sudo sysctl -w fs.file-max=100000
```

To make the modification persistent, create a file in `/etc/sysctl.d`

```sh
sudo touch 00-custom-settings.conf

vim 00-custom-settings.conf

fs.file-max=100000
```

The file will be read after reboot or you can run `sudo sysctl -p`
