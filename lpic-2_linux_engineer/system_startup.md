# System Startup

## Managing SysV init

tools

* who
* init
* chkconfig
* service

Configuration files are located in `/etc/rc.d` or directly in `/etc`.

Each run-level has it's own conf directory like `/etc/rc.d/rc5.d` or `/etc/rc5.d`

Check run-level info

```sh
sudo vim /etc/inittab
```

Show current run-level

```sh
who -r
```

Change run-level

```sh
sudo init 3
```

Turn service on or off

```sh
sudo chkconfig $SERVICE on
sudo chkconfig httpd on
```

List service statuses for each run-level

```sh
sudo chkconfig --list
```

Turn service on or off for specific run-levels

```sh
# Turn on for level 3 and 5
sudo chkconfig --level 35 httpd on
```

The `service` command is used to start and stop services

## System Recovery with GRUB

tools

* update-grub
* grub-mkconfig
* lsblk
* blkid

GRUB configuration is made in `/etc/default/grub` and `/etc/grub.d`

To update GRUB after configuration you can run

```sh
sudo update-grub
# Older distros
sudo update-grub2
# Red hat
sudo grub-mkconfig -o /boot/grub/grub.cfg
```

If GRUB can't boot an operating system you may have to boot from a live usb image.

You can then follow these steps to reinstall GRUB.

List mounted partitions

```sh
sudo lsblk
# More info
sudo lsblk -f
# Mount the partition
sudo mkdir /mnt/sdb2
sudo mount /dev/sdb2 /mnt/sdb2

grub-install --root-directory=/mnt/nvme0n1p3 /dev/nvme0n1
```

## Customizing the Initial RAM Disk

tools

* mkinitramfs
* mkinitrd
* unmkinitramfs

The init RAM disk image is located in `/boot`. In Ubuntu it's called `/boot/initrd.img` and in Red Hat `/boot/initramfs.img`

Create init RAM Disk image

```sh
# Ubuntu
mkinitramfs
# Red Hat
mkinitrd
```

Extract RAM disk

```sh
mkdir $HOME/tmp && cd $HOME/tmp
cp /boot/initrd.img .
unmkinitramfs ./initrd.img ./

# The operating system loaded by the image is located in the main directory
ls -la ./main/
```

Update modules loaded

```sh
sudo vim /etc/initramfs-tools/modules
# Example entries:
# raid10
# btrfs
sudo update-initramfs -u -b $HOME
```

Make GRUB use a custom image

Copy menuentry from `/boot/grub/grub.cfg`

```sh
menuentry 'Ubuntu' ...
    ...
    initrd  /boot/initrd.img-5.8...
}
```

Paste the menunentry into `/etc/grub.d/40_custom` and change the `initrd` image to your own.

```sh
sudo update-grub
```
